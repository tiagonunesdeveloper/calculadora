package calculadora

import (
	"fmt"
)

// Calculadora ...
func Calculadora(x, y int) string {
	return fmt.Sprintf("A soma de %d + %d é = %d", x, y, (x + y))
}
